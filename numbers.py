import math

def calculate_expression(a, b):
    result = (12 * a + 25 * b) / (1 + a**(2**b))
    rounded_result = round(result, 2)
    return rounded_result

a = float(input("Enter a value for 'a': "))
b = float(input("Enter a value for 'b': "))
result = calculate_expression(a, b)
print(result)
